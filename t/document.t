#! /usr/bin/env perl6

use v6.c;

use Pod::To::Man;
use Test;

plan 1;

subtest "Empty document" => {
	plan 1;

	is Pod::To::Man.render(""), "", "Empty Pod document returns empty troff document";
}

subtest "Simple document" => {
	dd Pod::To::Man.render($=pod);
}

=begin pod

=NAME Test Document
=AUTHOR Patrick Spek
=VERSION 0.1.2

=head1 Some test heading

And a test paragraph to go with it.

=item1 Why not a list item?
=item1 Or two?

Imagine paragraphs being here.

Seperated by whitespace.

=end pod

# vim: ft=perl6 noet
