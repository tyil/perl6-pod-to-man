#! /usr/bin/env false

use v6.c;

use Pod::To::Anything;

unit class Pod::To::Man does PodFormatting;

multi method render (Pod::Block::Named::Name:D $pod --> Str) { ".TH {self.pod-contents($pod)}\n" }
multi method render (Pod::Block::Named::Pod:D $pod --> Str) { self.pod-contents($pod) }
multi method render (Pod::Block::Para:D $pod --> Str) { "{self.pod-contents($pod)}\n\n" }
multi method render (Pod::Heading:D $pod --> Str) { ".SH {self.pod-contents($pod)}\n" }
multi method render (Pod::Item:D $pod --> Str) { "\\- {self.pod-contents($pod)}\n" }
multi method render (Str:D $pod --> Str) { $pod.subst(" - ", " \\- ", :g) }

multi method render (Pod::Block::Code:D $ --> Str) { "" }
multi method render (Pod::Block::Declarator:D $ --> Str) { … }
multi method render (Pod::Block::Named::Author:D $ --> Str) { … }
multi method render (Pod::Block::Named::Subtitle:D $ --> Str) { … }
multi method render (Pod::Block::Named::Title:D $ --> Str) { … }
multi method render (Pod::Block::Named::Version:D $ --> Str) { … }
multi method render (Pod::Block::Table:D $ --> Str) { … }

=begin pod

=NAME    Pod::To::Man
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 EXAMPLES

=head1 SEE ALSO

=end pod

# vim: ft=perl6 noet
